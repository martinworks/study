var slideFunc = {

	init : function(){
		this.ul = $(".visual_list");
		this.li = $(">li", this.ul);
		this.btn = $(">button", this.li);
		this.onImg = $("[data-slide-img='on']");
		this.offImg = $("[data-slide-img='off']");


		//기능 실행
		this.func();

	},

	func : function(){
		var that = this;
		var flag = true;
		var duration = 150;
		this.btn.click( function(){
			var me = $(this);
			setTimeout( function(){
				that.li.removeClass("on");
				that.li.find("button").stop().animate({"width":164+ "px"})
				me.parent().addClass("on");
				me.stop().animate({ "width": 666 + "px"})
			},duration )
		})

	}

}

$(document).ready( function(){

	slideFunc.init();

});